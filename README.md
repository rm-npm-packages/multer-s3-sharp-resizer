# Multer S3 Sharp Resizer

Multer S3 Sharp Resizer is a storage engine for multer.

It addresses a common problem you run into when creating a website that should utilize different sizes of images previously uploaded. You can easily setup the storage engine to create multiple versions of an image that has been uploaded.

Multer S3 Sharp Resizer is written completely in TypeScript and thus all types are provided with it.

## Setup

To set up the Storage engine with multer you need to create it like this. Now I will be using TypeScript in the following sections but you can set it up with JavaScript just as easy.

Here we want Multer S3 to upload a thumbnail image in addition to the original image. So the configuration of multer would look like this

**my-multer.ts**
```ts
import AWS from "aws-sdk"
import multer from "multer"
import S3SharpStorage from "multer-s3-sharp-resizer";
import myS3Config from "./somewhere.ts";

const storage = new S3SharpStorage({
  s3: new AWS.S3(myS3Config),
  bucket: 'my-bucket',
  imageFormats: [
    {
      fileFormat: 'jpg',
      folder: 'thumbnails'
      resize: {
        width: 100,
        height: 100
      }
    }
  ]
});

export default multer({
  storage
});
```

Configuring the route is just the same as in any regular multer setup

**route.ts**
```ts
import Multer from "./my-multer";
import express from "express";

const router = express.Router();

router.post('/',
  multer.single('image'),
  (req: Request, res: Response): => {
    res.status(201).json(req.file)
  }
)
```

For additional scenarios please see the multer documentation


## Options

And just like this the storage engine is setup. Now there are multiple Options you can choose from.

| Option              | Default                   | Type                              | Mandatory | Description                                                       |
| ------------------- | ------------------------- | --------------------------------- | --------- | ----------------------------------------------------------------- |
| s3                  | undefined                 | AWS.S3                            | yes       | The S3 Service object from aws-sdk                                |
| bucket              | undefined                 | string                            | yes       | Bucket you want your files to be stored in                        |
| uploadOriginalImage | true                      | boolean                           | no        | Set to false if you don't want your original image to be uploaded |
| metadata            | See [Metadata](#metadata) | Function                          | no        | Function to add metadate to your files.                           |
| keybase             | see [Key Base](#key-base)   | Function                          | no        | Function to create a key for your file                            |
| imageFormats        | []                        | see [Image Formats](#image-formats) | no        | The different Image Formats you want to upload                    |


### Metadata

If you want to add Metadata to your files you can define the metadata function. It returns an object of type Record<string,string> which will be added as key value metadata pairs to your file. 

 An example would look like this if we wanted to add a specific category to metadata that was sent in the query params of the http request.


**my-multer.ts**
```ts
const storage = new S3SharpStorage({
  s3: new AWS.S3(myS3Config),
  bucket: 'my-bucket',
  imageFormats: [
    {
      fileFormat: 'jpg',
      folder: 'thumbnails',
      resize: {
        width: 100,
        height: 100
      }
    }
  ],
  metadata: (req: Request, file: Express.Multer.File) => {
    return {
      category: req.query.category
    }
  }
});
```

### Key Base

By default the Key will be generated as a 16 character hex hash by crypto. However you can replace it with your own function like this. Remember that as we can upload different file formats in this case the key will be **without** a file ending. In this case we might want to change that instead of setting a category in metadata we want the image to be in the corresponding folder. So this image would in the end go to

*bucket/thumbnails/[category]/[key].jpg*

**my-multer.ts**
```ts
const storage = new S3SharpStorage({
  s3: new AWS.S3(myS3Config),
  bucket: 'my-bucket',
  uploadOriginalImage: false,
  imageFormats: [
    {
      fileFormat: 'jpg',
      folder: 'thumbnails',
      resize: {
        width: 100,
        height: 100
      }
    }
  ],
  keyBase: (req: Request, file?: Express.Multer.File) => {
    const hash = crypto.randomBytes(16).toString('hex')
    return `${req.query.category}/${hash}`
  }
});

```

### Image Formats

Now we already saw the image formats briefly in the previous sections. Each format determines what file type will be uploaded etc. 

| Option     | Default   | Type                 | Mandatory | Description                                                                                                 |
| ---------- | --------- | -------------------- | --------- | ----------------------------------------------------------------------------------------------------------- |
| fileFormat | undefined | 'png', 'jpg', 'webp' | yes       | The file type you want to be saved                                                                          |
| folder     | ''        | string               | no        | The folder you want your image to be saved in                                                               |
| resize     | undefined | sharp.ResizeOptions  | no        | Sharp Configuration for your resize. See [sharps documentation](https://sharp.pixelplumbing.com/api-resize) |


You can use all the options for resize that are provided by their [API](https://sharp.pixelplumbing.com/api-resize)



## Deletion of Images

Now the Storage engine comes with a removeKey method which will make sure that if you want to delete an image you just need to provide one key. Now as you most likely will make a request from a website this method will be called including a file extension. It doesn't matter though which one you provide given that you maybe decided to opload webp's as well as jpg's. Multer S3 Storage Engine will find it. To delete a key just implement it like this in your route


**my-multer.ts**
```ts
import AWS from "aws-sdk"
import multer from "multer"
import S3SharpStorage from "multer-s3-sharp-resizer";
import myS3Config from "./somewhere.ts";

const storage = new S3SharpStorage({
  s3: new AWS.S3(myS3Config),
  bucket: 'my-bucket',
  imageFormats: [
    {
      fileFormat: 'jpg',
      folder: 'thumbnails'
      resize: {
        width: 100,
        height: 100
      }
    }
  ]
});

export { storage }

export default multer({
  storage
});
```


**route.ts**
```ts
import { storage } from "./my-multer"

const router = express.Router();

router.delete('/:Filename', async (req: Request, res: Response): => {
    const deleted = await storage.removeKey(req.params.Filename);
    res.status(200).json(deleted)
  }
)
```
