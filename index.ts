import MulterS3SharpStorage, {
  DeletionResponseItem,
  ImageConversionType,
  Metadata,
  MulterS3SharpStoragaOptions,
  UploadInput,
  UploadResult
} from './src/index';


export {
  DeletionResponseItem,
  ImageConversionType,
  Metadata,
  MulterS3SharpStoragaOptions,
  UploadInput,
  UploadResult
};

export default MulterS3SharpStorage;