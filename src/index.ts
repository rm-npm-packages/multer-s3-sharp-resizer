import MulterS3SharpStorage from './classes/class.MulterS3SharpStorage';
import MulterS3SharpStoragaOptions from './classes/interface.MulterS3StorageOptions';
import ImageConversionType from './classes/interface.ImageConversionType';
import DeletionResponseItem from './classes/interface.DeletionResponseItem';
import UploadInput from './classes/interface.UploadInput';
import Metadata from './classes/type.Metadata';
import UploadResult from './classes/interface.UploadResult';

export default MulterS3SharpStorage;

export { 
  MulterS3SharpStoragaOptions,
  ImageConversionType,
  DeletionResponseItem,
  UploadInput,
  Metadata,
  UploadResult
};