import ImageConversionType from './interface.ImageConversionType';

export default interface UploadInput {
  bucket: string;
  file: Express.Multer.File  
  key?: string;
  s3: AWS.S3;
  imgOpts?: ImageConversionType;
}