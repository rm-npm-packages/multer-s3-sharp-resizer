import { Request } from 'express';

type GetFilenameBase = (req:Request, file?: Express.Multer.File) => string;


export default GetFilenameBase;