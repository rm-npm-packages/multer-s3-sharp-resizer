import ImageConversionType from './interface.ImageConversionType';
import GetFilenameBase from './type.GetFilenameBase';
import Metadata from './type.Metadata';

export default interface MulterS3SharpStorageOptions {
  meta?: Record<string,any>,
  imageFormats?: Array<ImageConversionType>,
  uploadOriginalImage?: boolean,
  keyBase?: GetFilenameBase
  s3: AWS.S3;
  bucket: string;
  metadata: Metadata
}