export default interface DeletionResponseItem {
  filename: string;
  folder?: string;
}