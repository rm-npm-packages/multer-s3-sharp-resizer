import sharp from 'sharp';

export default interface ImageConversionType {
  resize?: sharp.ResizeOptions,  
  folder?: string;
  fileFormat: 'png' | 'jpg' | 'webp'
}