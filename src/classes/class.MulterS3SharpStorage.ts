import { Request } from 'express';
import { StorageEngine } from 'multer';
import AWS from 'aws-sdk';
import {  Writable } from 'stream';
import path from 'path';
import ImageConversionType from './interface.ImageConversionType';
import GetFilenameBase from './type.GetFilenameBase';
import Metadata from './type.Metadata';
import MulterS3SharpStorageOptions from './interface.MulterS3StorageOptions';
import getDefaultFilenameBase from '../services/service.getDefaultFilenameBase';
import UploadResult from './interface.UploadResult';
import uploadFromStream from '../services/service.uploadFromStream';
import convertImage from '../services/service.convertImage';
import DeletionResponseItem from './interface.DeletionResponseItem';
import removeFileExtension from '../services/service.removeFileExtension';


type AnyFunction = (...any) => any;

export default class MulterS3SharpStorage implements StorageEngine {
  imageFormats: Array<ImageConversionType>;
  uploadOriginalImage = true;
  keyBase: GetFilenameBase;
  s3: AWS.S3;
  bucket: string;
  metadata: Metadata = () => { return {}; };

  constructor(input:MulterS3SharpStorageOptions){
    this.imageFormats = input.imageFormats || [];
    this.uploadOriginalImage = input.uploadOriginalImage !== undefined ? input.uploadOriginalImage : true;
    this.keyBase = input.keyBase || getDefaultFilenameBase;
    this.s3 = input.s3;
    this.bucket = input.bucket;
    this.metadata = input.metadata || this.metadata;
  }
  
  async _handleFile(req:Request, file: Express.Multer.File, cb:AnyFunction):Promise<void>{
    const originalImage = file.stream;    
    const pipes: Array<Writable> = [];
    const uploadResults: Array<UploadResult> = [];
    const baseKey = this.keyBase(req, file);
    
    if (this.uploadOriginalImage){
      const originalImageUpload = await uploadFromStream(req, { 
        s3: this.s3, 
        bucket: this.bucket,
        file, 
        key: `${baseKey}${path.extname(file.originalname)}`
      }, this.keyBase, this.metadata);

      uploadResults.push(originalImageUpload);
      pipes.push(
        originalImage.pipe(originalImageUpload.stream)
      );
      
    }

    for (const imageSettings of this.imageFormats){
      const resizeImageUpload = await uploadFromStream(req, {
        key: `${imageSettings.folder + '/' || ''}${baseKey}.${imageSettings.fileFormat}`,
        file,
        s3: this.s3,
        bucket: this.bucket
      }, this.keyBase, this.metadata);

      uploadResults.push(resizeImageUpload);

      pipes.push(originalImage
        .pipe(convertImage(imageSettings))
        .pipe(resizeImageUpload.stream)
      );
    }


    const uploadedFiles = await Promise.all(uploadResults.map(ur => ur.upload));
    cb(null, {
      uploadedFiles,
      key: baseKey
    }); 
  }


  async removeKey(key:string):Promise<DeletionResponseItem[]>{
    const deleteFiles:any[] = [];
    const returnedResult:DeletionResponseItem[] = [];

    if (this.uploadOriginalImage){
      const deletion = this.s3.deleteObject({
        Bucket: this.bucket,
        Key: key
      }).promise();

      deleteFiles.push(deletion);
      await deletion;
      returnedResult.push({
        filename: key
      });
    }

    for (const format of this.imageFormats){
      const deletion = this.s3.deleteObject({
        Bucket: this.bucket,
        Key: `${format.folder + '/' || ''}${removeFileExtension(key)}.${format.fileFormat}`
      }).promise();
      deleteFiles.push(deletion);
      await deletion;
      returnedResult.push({
        filename: key,
        folder: format.folder
      });
    }

    try {
      await Promise.all(deleteFiles);
      return returnedResult;
    } catch (err) {
      throw err;
    }    
  }

  _removeFile(req: Request,file: Express.Multer.File, cb: (error: Error) => void):void{
  }
}